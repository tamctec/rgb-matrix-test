from PIL import Image
import sys

im = Image.open(sys.argv[1])
print(im.size)
im = im.crop((17, 17, 100-17, 100-17))
im = im.resize((16, 16))
px = im.load()
width, height = im.size
temp = []
for y in range(height):
    line = []
    for x in range(width):
        r,g,b,a = px[x, y]
        if a == 0:
            h = -1
        else:
            h = (r<<16)+(g<<8)+b
        line.append(h)
        # print("0x%06X"%h)
    temp.append(line)
print(temp)