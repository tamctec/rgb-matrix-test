# RGB Matrix Test

![smile](imgs/smile.jpeg)

RGB Matrix test with 2x2 IRM mini. Examples uses micropython, you can use Raspberry Pi Pico, ESP32 or ESP8266. But currently only test on Raspberry Pi Pico.

## Wiring

The code wire `Data` pin to Pico GPIO0.

![smile](imgs/irm-mini-4x4-back.png)
4 IRM mini wires like above.

## Usage

Copy `font_map.py` `irm.py`, `sprite_map.py`, and `ws2812.py` to device root from [libs of IRM mini](https://gitlab.com/tamctec/irm-mini/-/tree/main/libs). Then copy examples you want to the root, and run it.

> For ESP8266 and ESP32, you don't need `ws2812.py`, as it's built in the firmwire.