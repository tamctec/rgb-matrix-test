from machine import Pin
import time
from irm import IRM

irm = IRM(Pin(0), [[1, 2],
                    [4, 3]])
rows = 2*8*2-1
_rows = rows-1
_half_rows = int(_rows/2)
RAINBOW = [360/rows*v for v in range(rows)]
irm.brightness = 10

def main():
    offset = 0
    while True:
        for i in range(rows):
            line = i+offset
            if line > _rows:
                line = line-_rows
            if i < _half_rows:
                irm.draw_line(0, i, i, 0, irm.hue2rgb(RAINBOW[line]))
            else:
                irm.draw_line(i-_half_rows, _half_rows, _half_rows, i-_half_rows, irm.hue2rgb(RAINBOW[line]))
        irm.write()
        time.sleep(0.01)
        offset += 1
        if offset > _rows:
            offset = 0
        time.sleep(0.01)

if __name__ == "__main__":
    main()
