from machine import Pin
import time
from irm import IRM

irm = IRM(Pin(0), [[1, 2],
                    [4, 3]])
irm.brightness = 10
irm.fill([255, 255, 255])
    
def heart_face_1():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(3, 4, "SMALL_HEART")
    irm.draw_sprite(9, 4, "SMALL_HEART")
    irm.draw_sprite(3, 9, "LARGH_MOUTH")
    irm.write()
def heart_face_2():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(2, 3, "BIG_HEART")
    irm.draw_sprite(8, 3, "BIG_HEART")
    irm.draw_sprite(3, 9, "LARGH_MOUTH")
    irm.write()
def smile_face_1():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(4, 4, "NORMAL_EYE")
    irm.draw_sprite(9, 4, "NORMAL_EYE")
    irm.draw_sprite(3, 10, "SMILE_MOUTH")
    irm.write()
def wink_face_1():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(4, 4, "NORMAL_EYE")
    irm.draw_sprite(9, 5, "CLOSE_EYE")
    irm.draw_sprite(3, 10, "SMILE_MOUTH")
    irm.write()
def close_eyes_face_1():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(3, 5, "CLOSE_EYE")
    irm.draw_sprite(9, 5, "CLOSE_EYE")
    irm.draw_sprite(3, 10, "SMILE_MOUTH")
    irm.write()
def cry_face_1():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(2, 2, "SAD_LEFT_EYEBROW")
    irm.draw_sprite(9, 2, "SAD_RIGHT_EYEBROW")
    irm.draw_sprite(4, 4, "NORMAL_EYE")
    irm.draw_sprite(9, 4, "NORMAL_EYE")
    irm.draw_sprite(3, 10, "SAD_MOUTH")
    irm.write()
def cry_face_2():
    irm.draw_circle(7, 7, 7, [255, 200, 0], fill=True)
    irm.draw_sprite(2, 2, "SAD_LEFT_EYEBROW")
    irm.draw_sprite(9, 2, "SAD_RIGHT_EYEBROW")
    irm.draw_sprite(3, 5, "CLOSE_EYE")
    irm.draw_sprite(9, 5, "CLOSE_EYE")
    irm.draw_sprite(3, 10, "SAD_MOUTH")
    irm.write()

def love():
    heart_face_1()
    time.sleep(0.2)
    heart_face_2()
    time.sleep(0.2)
    heart_face_1()
    time.sleep(0.2)
    heart_face_2()
def wink():
    smile_face_1()
    time.sleep(0.5)
    wink_face_1()
    time.sleep(0.01)
    smile_face_1()
def blink():
    smile_face_1()
    time.sleep(0.5)
    close_eyes_face_1()
    time.sleep(0.01)
    smile_face_1()
    time.sleep(0.5)
    close_eyes_face_1()
    time.sleep(0.01)
    smile_face_1()
def cry():
    cry_face_1()
    time.sleep(0.5)
    cry_face_2()
    time.sleep(0.01)
    cry_face_1()
    time.sleep(0.5)
    cry_face_2()
    time.sleep(0.01)
    cry_face_1()

def main():
    love()
    time.sleep(2)
    wink()
    time.sleep(2)
    blink()
    time.sleep(2)
    cry()
    time.sleep(2)

if __name__ == "__main__":
    main()
