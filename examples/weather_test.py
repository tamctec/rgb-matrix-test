import urequests
from irm import IRM
from machine import Pin
from weather_sprites import WEATHER_SPRITES
import time
import json

data = {
    "api_key": "YOUR_API_KEY_HERE",
    "city_name": "YOUR_CITY_NAME_HERE",
    "units": "metric",
}
weather_url = "https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={api_key}&units={units}"


irm = IRM(Pin(13), [[1, 2],
                    [4, 3]])
irm.brightness = 50

res = urequests.get(url=weather_url.format(**data))
data = json.loads(res.text)
print(data)
weather_icon = data["weather"][0]["icon"]
temp = data["main"]["temp"]
irm.clear()
irm.draw_sprite(0, 0, WEATHER_SPRITES[weather_icon])
irm.draw_text(0, 0, temp, irm.FONT5, [0, 255, 100])
irm.write()
